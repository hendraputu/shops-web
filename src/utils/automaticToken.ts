import axios from 'axios';
import { getAuthCookie } from './functions';

export const automaticToken = () => {
  // set interceptor for axios to use auth token stored in key
  // https://stackoverflow.com/questions/43051291/attach-authorization-header-for-all-axios-requests
  axios.interceptors.request.use(config => {
    const { access_token } = getAuthCookie();
    // console.log('url', config.url);
    // Only add Authorization for api call, not for upload file directly to s3. S3 do not accept Authorization headers
    if (config.headers['Flag'] !== 'Upload') {
      config.headers.Authorization = `Bearer ${access_token}`;
    }
    return config;
  });
};