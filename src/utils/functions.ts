import axios from "axios";
import { set, get } from "js-cookie";
import { URL } from "../constants/api";
import { ILoginData, IAuth, initialAuth, IUser } from "../utils/types";

// login
export const login = (loginData: ILoginData) => {
  return axios.post(`${URL}/login`, loginData);
};

export const setAuthCookie = (auth: IAuth) => {
  set("auth", auth, { expires: 30 });
};

export const clearAuthCookie = () => {
  set("auth", { ...initialAuth });
};

export const getAuthCookie = (): IAuth => {
  const authCookie = get("auth");
  if (authCookie) {
    return JSON.parse(authCookie);
  }
  return initialAuth;
};
// product
export const getProductCustomer = () => {
  return axios.get(`${URL}/products`);
};

export const getProducts = () => {
  return axios.get(`${URL}/product-list`);
};
// user
export const getUsers = () => {
  return axios.get(`${URL}/user`);
};

export const getUser = (id: number) => {
  return axios.get(`${URL}/user/${id}`);
};

export const createUser = (user: IUser) => {
  return axios.post(`${URL}/user`, user);
};

export const updateUser = (user: IUser, id: number) => {
  return axios.put(`${URL}/user/${id}`, user);
};

export const deleteUser = (id: number) => {
  return axios.delete(`${URL}/user/${id}`);
};
