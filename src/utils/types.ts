export interface ITimestamp {
  created_at: string;
  updated_at: string;
}

export interface IUser extends ITimestamp {
  id: number;
  email: string;
  is_admin: number;
  is_warehouse: number;
  status: string;
}

export interface IAuth {
  access_token: string;
  user: IUser;
}

export const initialAuth: IAuth = {
  access_token: "",
  user: {
    id: 0,
    email: "",
    is_admin: 0,
    is_warehouse: 0,
    status: "",
    created_at: "",
    updated_at: "",
  },
};

export interface ILoginData {
  email: string;
  password: string;
}

export interface IProduct extends ITimestamp {
  id: number;
  brand_id: number;
  cpu: number;
  name: string;
  os: string;
  photo: string;
  price: number;
  ram: number;
  status: string;
  stock: number;
  deleted_at: string;
}

export interface IBrand {
  id: number;
  name: string;
}

export interface IOrder extends ITimestamp {
  id: number;
  user_id: number;
  total_price: number;
  invoice_number: string;
  status: string;
}

export interface IOrderProduct extends ITimestamp {
  id: number;
  order_id: number;
  product_id: number;
  quantity: number;
}
