import React, { useEffect, useState, createContext, useContext } from "react";
import Main from "./pages";
// import RegisterForm from "../pages/RegisterForm";
import LoginForm from "./pages/LoginForm";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { IAuth, initialAuth } from "./utils/types";
import { getAuthCookie, setAuthCookie } from "./utils/functions";
import "bootstrap/dist/css/bootstrap.min.css";
import { automaticToken } from "./utils/automaticToken";

export const AuthContext = createContext<[IAuth, any]>([initialAuth, null]);

const App = () => {
  const [auth, setAuth] = useState<IAuth>(getAuthCookie());
  const [products, setProducts] = useState([]);

  useEffect(() => {
    setAuthCookie(auth);
  }, [auth]);

  console.log("Auth is:");
  console.log(auth);

  automaticToken();
  return (
    <AuthContext.Provider value={[auth, setAuth]}>
      <BrowserRouter>
        <Switch>
          <Route path="/login">
            <LoginForm />
          </Route>
          {/* <Route path="/register" component={RegisterForm} /> */}
          <Route path="/main" component={Main} />
          <Redirect from="/" to="main" />
        </Switch>
      </BrowserRouter>
    </AuthContext.Provider>
  );
};

export default App;
