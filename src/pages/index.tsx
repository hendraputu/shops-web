import * as React from "react";
import NavigationBar from "../components/NavigationBar";
import { Switch, Route } from "react-router-dom";
import ProductList from "./product/ProductList";
import OrderList from "./order/OrderList";
import UserList from "./user/UserList";

const Main = () => {
  return (
    <React.Fragment>
      <NavigationBar />
      <div>&nbsp;</div>
      <Switch>
        <Route path="/main/product">
          <ProductList />
        </Route>
        <Route path="/main/order">
          <OrderList />
        </Route>
        <Route path="/main/user">
          <UserList />
        </Route>
      </Switch>
    </React.Fragment>
  );
};

export default Main;
