// import * as React from "react";
import React, { useState } from "react";
import RichTextEditor from "react-rte";

const OrderList = () => {
  const [value, setValue] = useState(RichTextEditor.createEmptyValue());

  console.log("value:");
  let nilai = value.toString("html");
  console.log(nilai);

  // static propTypes = {
  //   onChange: PropTypes.func
  // };

  // state = {
  //   value: RichTextEditor.createEmptyValue()
  // }

  // onChange = (value) => {
  //   this.setState({value});
  //   if (this.props.onChange) {
  //     // Send the changes up to the parent component as an HTML string.
  //     // This is here to demonstrate using `.toString()` but in a real app it
  //     // would be better to avoid generating a string on each change.
  //     this.props.onChange(
  //       value.toString('html')
  //     );
  //   }
  // };
  return (
    <div>
      <h1>Order List</h1>
      <p>test rich text editor</p>
      <RichTextEditor value={value} onChange={setValue} />
    </div>
  );
};

export default OrderList;
