import React, { useEffect, useState } from "react";
import { getUsers } from "../../utils/functions";
import { IUser } from "../../utils/types";

const UserList = () => {
  const [users, setUsers] = useState<IUser[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    setIsLoading(true);
    getUsers()
      .then((res) => {
        console.log("User list:");
        console.log(res.data);
        setUsers(res.data);
        setIsLoading(false);
      })
      .catch(() => console.log("err get user list"));
  }, []);
  return (
    <div className="container-fluid">
      <h1>User List</h1>
      <div className="row">
        <div className="col">
          {isLoading && (
            <div className="progress">
              <div
                className="progress-bar progress-bar-striped progress-bar-animated"
                role="progressbar"
                style={{ width: "100%" }}
              ></div>
            </div>
          )}

          <table className="table table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>email</th>
                <th>is admin</th>
                <th>is warehouse</th>
                <th>status</th>
                <th>action</th>
              </tr>
            </thead>
            <tbody>
              {users &&
                users.map((item) => (
                  <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.email}</td>
                    <td>{item.is_admin}</td>
                    <td>{item.is_warehouse}</td>
                    <td>{item.status}</td>
                    <td>edit | delete</td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
      </div>
      <h2>Create User</h2>
      <div className="row">
        <div className="col">
          <form action="">
            <div className="form-group">
              <label>Email</label>
              <input type="email" className="form-control" />
            </div>
            <div className="form-group">
              <label>Password</label>
              <input type="password" className="form-control" />
            </div>
            <div className="form-group">
              <label>Password Confirmation</label>
              <input type="password" className="form-control" />
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default UserList;
