import React, { useEffect, useState } from "react";
import { getProducts } from "../../utils/functions";
import { IProduct } from "../../utils/types";

const ProductList = () => {
  const [products, setProducts] = useState<IProduct[]>([]);
  useEffect(() => {
    getProducts()
      .then((res) => {
        console.log("Product list:");
        console.log(res.data);
        setProducts(res.data);
      })
      .catch(() => console.log("err get product list"));
  }, []);
  return (
    <div>
      <h1>Product List</h1>
      <ul>{products && products.map((item) => <li>{item.name}</li>)}</ul>
    </div>
  );
};

export default ProductList;
