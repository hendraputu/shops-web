import React, { useState, useContext } from "react";
// import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { AuthContext } from "../App";
import { login } from "../utils/functions";
import { IAuth, ILoginData } from "../utils/types";
import { Link } from "react-router-dom";
interface ILoginForm {
  setAccess: (value: IAuth) => void;
}
const LoginForm = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [auth, setAuth] = useContext(AuthContext);

  const handleEmail = (event: any) => {
    setEmail(event.target.value);
  };

  const handlePassword = (event: any) => {
    setPassword(event.target.value);
  };

  const submit = () => {
    console.log("info login");
    console.log(email);
    console.log(password);
    const loginData: ILoginData = {
      email,
      password,
    };
    login(loginData)
      .then((res) => {
        console.log("login success");
        console.log(res);
        setAuth(res.data);
      })
      .catch(() => console.log("err login"));
  };

  return (
    <div>
      <input type="email" name="email" onChange={handleEmail} />
      <input type="password" name="password" onChange={handlePassword} />
      <button onClick={submit}>Login</button>
      <Link to="/main">Main</Link>
    </div>
  );
};

export default LoginForm;
